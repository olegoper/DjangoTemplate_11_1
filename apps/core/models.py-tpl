# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.urls import reverse
from django.utils.encoding import python_2_unicode_compatible


class TimestampModel(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


@python_2_unicode_compatible
class MyGeneric(TimestampModel):
    text_field = models.TextField()
    int_field = models.IntegerField()

    def get_absolute_url(self):
        return reverse('core:mygeneric-detail', kwargs={'pk': self.id})

    def __str__(self):
        return self.text_field + ": " + str(self.int_field)
